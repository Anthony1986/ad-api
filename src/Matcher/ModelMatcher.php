<?php

namespace App\Matcher;

use App\Entity\Model;

class ModelMatcher implements ModelMatcherInterface
{
    public function findMatchingModel(string $model, array $arrayModels): ?Model
    {
        $model = strtolower($model);
        $bestMatch = null;
        $bestSimilarity = 0;

        foreach ($arrayModels as $value) {
            $valueWithoutSpaces = strtolower(str_replace(' ', '', $value->getName()));

            if (str_replace(' ', '', $model) === $valueWithoutSpaces) {
                return $value;
            }

            $modelWords = explode(' ', $model);
            foreach ($modelWords as $modelWord) {
                similar_text($modelWord, $valueWithoutSpaces, $wordSimilarity);

                if ($wordSimilarity > $bestSimilarity) {
                    $bestMatch = $value;
                    $bestSimilarity = $wordSimilarity;
                }
            }
        }

        if ($bestSimilarity < 75) {
            return null;
        }

        return $bestMatch;
    }
}
