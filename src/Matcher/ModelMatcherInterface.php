<?php

namespace App\Matcher;

use App\Entity\Model;

interface ModelMatcherInterface
{
    public function findMatchingModel(string $model, array $arrayModels): ?Model;
}
