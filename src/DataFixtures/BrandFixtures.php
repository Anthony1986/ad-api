<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BrandFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $brandsNames = ['Audi', 'BMW', 'Citroen'];

        foreach ($brandsNames as $brandName) {
            $brand = (new Brand())->setName($brandName);

            $manager->persist($brand);
        }

        $manager->flush();
    }
}
