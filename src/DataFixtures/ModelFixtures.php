<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Model;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ModelFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $cars = [
            [
                'brand' => 'Audi',
                'model' => 'Cabriolet',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Q2',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Q3',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Q5',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Q7',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Q8',
            ],
            [
                'brand' => 'Audi',
                'model' => 'R8',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Rs3',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Rs4',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Rs5',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Rs7',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S3',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S4',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S4 Avant',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S4 Cabriolet',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S5',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S7',
            ],
            [
                'brand' => 'Audi',
                'model' => 'S8',
            ],
            [
                'brand' => 'Audi',
                'model' => 'SQ5',
            ],
            [
                'brand' => 'Audi',
                'model' => 'SQ7',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Tt',
            ],
            [
                'brand' => 'Audi',
                'model' => 'Tts',
            ],
            [
                'brand' => 'Audi',
                'model' => 'V8',
            ],
            [
                'brand' => 'BMW',
                'model' => 'M3',
            ],
            [
                'brand' => 'BMW',
                'model' => 'M4',
            ],
            [
                'brand' => 'BMW',
                'model' => 'M5',
            ],
            [
                'brand' => 'BMW',
                'model' => 'M535',
            ],
            [
                'brand' => 'BMW',
                'model' => 'M6',
            ],
            [
                'brand' => 'BMW',
                'model' => 'M635',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 1',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 2',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 3',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 4',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 5',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 6',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 7',
            ],
            [
                'brand' => 'BMW',
                'model' => 'Serie 8',
        ],
            [
                'brand' => 'Citroen',
                'model' => 'C1',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C15',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C2',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C25',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C25D',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C25E',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C25TD',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C3',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C3 Aircross',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C3 Picasso',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C4',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C4 Picasso',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C5',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C6',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'C8',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'Ds3',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'Ds4',
            ],
            [
                'brand' => 'Citroen',
                'model' => 'Ds5',
            ],
        ];
        foreach ($cars as $car) {
            $brand = $manager->getRepository(Brand::class)->findOneBy(['name' => $car['brand']]);

            $model = (new Model())
                ->setName($car['model'])
                ->setBrand($brand);

            $manager->persist($model);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            BrandFixtures::class,
        ];
    }
}
