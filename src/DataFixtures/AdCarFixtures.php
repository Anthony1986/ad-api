<?php

namespace App\DataFixtures;

use App\Entity\AdCar;
use App\Entity\Model;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AdCarFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {

        $model = $manager->getRepository(Model::class)->findOneBy(['name' => 'Rs4']);
        $dateTime = new DateTimeImmutable();

        $adCar = (new AdCar())
            ->setTitle('nouveau titre')
            ->setContent('nouveau contenu')
            ->setBrand($model->getBrand())
            ->setModel($model)
            ->setCreatedAt($dateTime)
            ->setUpdatedAt($dateTime)
        ;

        $manager->persist($adCar);
        $manager->flush();

    }

    public function getDependencies(): array
    {
        return [
            BrandFixtures::class,
            ModelFixtures::class,
        ];
    }
}
