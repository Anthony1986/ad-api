<?php

namespace App\Resolver;

use App\Factory\AdFactoryInterface;

interface AdFactoryResolverInterface
{
    public function resolve(string $type): AdFactoryInterface;
}
