<?php

namespace App\Resolver;

use App\Factory\AdFactoryInterface;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class AdFactoryResolver implements AdFactoryResolverInterface
{
    public function __construct(
        #[TaggedIterator('ad_factory')] protected iterable $adFactories,
    )
    {
    }

    public function resolve(string $type): AdFactoryInterface
    {
        foreach ($this->adFactories as $adFactory) {
            if ($adFactory->getType() === $type) {
                return $adFactory;
            }
        }

        throw new InvalidArgumentException(sprintf('Ad factory for type "%s" not found', $type));
    }
}
