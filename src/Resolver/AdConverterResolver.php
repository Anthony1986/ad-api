<?php

namespace App\Resolver;

use App\Converter\ConverterInterface;
use InvalidArgumentException;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class AdConverterResolver implements AdConverterResolverInterface
{
    public function __construct(
        #[TaggedIterator('ad_converter')] protected iterable $adConverters,
    )
    {
    }

    public function resolve(string $type): ConverterInterface
    {
        foreach ($this->adConverters as $adConverter) {
            if ($adConverter->getType() === $type) {
                return $adConverter;
            }
        }

        throw new InvalidArgumentException(sprintf('Ad converter for type "%s" not found', $type));
    }
}
