<?php

namespace App\Resolver;

use App\Converter\ConverterInterface;

interface AdConverterResolverInterface
{
    public function resolve(string $type): ConverterInterface;
}
