<?php

namespace App\Controller;

use App\Checker\JsonChecker;
use App\Entity\Ad;
use App\Resolver\AdConverterResolverInterface;
use App\Resolver\AdFactoryResolverInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdController extends AbstractController
{
    public function __construct(
        private readonly AdFactoryResolverInterface $adFactoryResolver,
        private readonly AdConverterResolverInterface $adConverterResolver,
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @throws Exception
     */
    #[Route('/ad/{id}', name: 'ad_get', methods: ['GET'])]
    public function get(Ad $ad): JsonResponse
    {
        $data = $this->adConverterResolver->resolve($ad->getType())->retrieveData($ad, []);

        return $this->json([
            'code' => Response::HTTP_OK,
            'message' => 'Ad retrieved successfully',
            'data' => $data,
        ], Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    #[Route('/ad-create', name: 'ad_create', methods: ['POST'])]
    public function create(Request $request): JsonResponse
    {
        $data = $this->threatJsonData($request->getContent());

        $adFactory = $this->adFactoryResolver->resolve($data['type']);
        $ad = $adFactory->create($data);

        $adConverter = $this->adConverterResolver->resolve($data['type']);

        return $this->json([
            'code' => Response::HTTP_CREATED,
            'message' => 'A new Ad has been created with success',
            'data' => $adConverter->retrieveData($ad, $data),
        ],
            Response::HTTP_CREATED);
    }

    /**
     * @throws Exception
     */
    #[Route('/ad-update/{id}', name: 'ad_update', methods: ['PUT'])]
    public function update(Request $request, Ad $ad): JsonResponse
    {
        $data = $this->threatJsonData($request->getContent());
        if ($ad->getType() !== $data['type']) {
            throw new Exception('You cannot change the type of an Ad', Response::HTTP_BAD_REQUEST);
        }

        $adFactory = $this->adFactoryResolver->resolve($data['type']);
        $adFactory->update($ad, $data);

        $adConverter = $this->adConverterResolver->resolve($data['type']);

        return $this->json([
            'code' => Response::HTTP_OK,
            'message' => 'Ad updated successfully',
            'data' => $adConverter->retrieveData($ad, $data),
        ], Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    #[Route('/ad-delete/{id}', name: 'ad_delete', methods: ['DELETE'])]
    public function delete(Ad $ad): JsonResponse
    {
        try {
            $this->entityManager->remove($ad);
            $this->entityManager->flush();
        } catch (Exception $e) {
            $this->logger->error('Error while deleting Ad', [
                'exception' => $e,
            ]);

            throw new Exception('Error while deleting Ad', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        return $this->json([
            'code' => Response::HTTP_OK,
            'message' => 'Ad deleted successfully',
        ], Response::HTTP_OK);
    }

    /**
     * @throws Exception
     */
    private function threatJsonData(string $content): array
    {
        $content = JsonChecker::checkJson($content);

        if (!is_array($content) || !array_key_exists('data', $content)) {
            $this->logger->error('Invalid JSON format or missing data', [
                'request' => $content,
            ]);

            throw new Exception('Invalid JSON format or missing data', Response::HTTP_BAD_REQUEST);
        }

        if (!array_key_exists('type', $content['data'])) {
            $this->logger->error('Missing required field type', [
                'request' => $content,
            ]);

            throw new Exception('Missing required field type', Response::HTTP_BAD_REQUEST);
        }

        return $content['data'];
    }
}
