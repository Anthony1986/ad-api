<?php

namespace App\Checker;

use Exception;

class EmptyChecker
{
    /**
     * @throws Exception
     */
    static function checkFieldsNotEmpty(array $data, array $fields): void
    {
        foreach ($fields as $field) {
            if (empty($data[$field])) {
                throw new Exception($field . ' cannot be empty');
            }
        }
    }
}
