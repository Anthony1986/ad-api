<?php

namespace App\Checker;

use Exception;

class JsonChecker
{
    /**
     * @throws Exception
     */
    static function checkJson(string $json, bool $assoc = true)
    {
        $decodedJson = json_decode($json, $assoc);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Invalid JSON: ' . json_last_error_msg());
        }

        return $decodedJson;
    }
}
