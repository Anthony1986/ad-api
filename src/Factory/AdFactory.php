<?php

namespace App\Factory;

use App\Entity\Ad;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class AdFactory
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
    )
    {
    }

    /**
     * @throws Exception
     */
    public function flush(string $type): void
    {
        try {
            $this->entityManager->flush();
        } catch (Exception $e) {
            throw new Exception('Error editing Ad ' . $type . ': ' . $e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @throws Exception
     */
    public function save(Ad $ad, string $type): void
    {
        try {
            $this->entityManager->persist($ad);
            $this->entityManager->flush();
        } catch (Exception $e) {
            throw new Exception('Error saving Ad ' . $type . ': ' . $e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * @throws Exception
     */
    protected function setCommonField(Ad $ad, array $data): void
    {
        $dateTime = new DateTimeImmutable();

        $ad->setTitle($data['title']);
        $ad->setContent($data['content']);
        $ad->setCreatedAt($dateTime);
        $ad->setUpdatedAt($dateTime);
    }

    /**
     * @throws Exception
     */
    protected function checkRequiredFields(array $data, array $requiredFields): void
    {
        foreach ($requiredFields as $requiredField) {
            if (!array_key_exists($requiredField, $data)) {
                throw new Exception('Missing required field ' . $requiredField);
            }
        }
    }
}
