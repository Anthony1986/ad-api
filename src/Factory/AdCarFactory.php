<?php

namespace App\Factory;

use App\Checker\EmptyChecker;
use App\Entity\Ad;
use App\Entity\AdCar;
use App\Matcher\ModelMatcherInterface;
use App\Provider\ModelProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

#[AutoconfigureTag('ad_factory', ['alias' => 'ad_car'])]
class AdCarFactory extends AdFactory implements AdFactoryInterface
{
    public function __construct(
        private readonly ModelProviderInterface $modelProvider,
        private readonly ModelMatcherInterface $modelMatcher,
        private readonly EntityManagerInterface $entityManager,
    )
    {
        parent::__construct($this->entityManager);
    }

    /**
     * @throws Exception
     */
    public function create(array $data): Ad
    {
        $this->checkRequiredFields($data, ['title', 'content', 'model']);
        EmptyChecker::checkFieldsNotEmpty($data, ['title', 'content', 'model']);

        $allModels = $this->modelProvider->getAllModels();
        $matchedModel = $this->modelMatcher->findMatchingModel($data['model'], $allModels);

        if (null === $matchedModel) {
            throw new NotFoundHttpException('Model not found');
        }

        $adCar = (new AdCar())
            ->setBrand($matchedModel->getBrand())
            ->setModel($matchedModel);
        $this->setCommonField($adCar, $data);

        $this->save($adCar, $this->getType());

        return $adCar;
    }

    /**
     * @throws Exception
     */
    public function update(Ad $ad, array $data): Ad
    {
        $this->checkRequiredFields($data, ['title', 'content', 'model']);
        EmptyChecker::checkFieldsNotEmpty($data, ['title', 'content', 'model']);

        $allModels = $this->modelProvider->getAllModels();
        $matchedModel = $this->modelMatcher->findMatchingModel($data['model'], $allModels);

        if (null === $matchedModel) {
            throw new NotFoundHttpException('Model not found');
        }

        $ad->setBrand($matchedModel->getBrand())
            ->setModel($matchedModel);
        $this->setCommonField($ad, $data);

        $this->flush($this->getType());

        return $ad;
    }

    public function getType(): string
    {
        return self::CAR;
    }
}
