<?php

namespace App\Factory;

use App\Checker\EmptyChecker;
use App\Entity\Ad;
use App\Entity\AdJob;
use Exception;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('ad_factory', ['alias' => 'ad_job'])]
class AdJobFactory extends AdFactory implements AdFactoryInterface
{
    /**
     * @throws Exception
     */
    public function create(array $data): Ad
    {
        $this->checkRequiredFields($data, ['title', 'content']);
        EmptyChecker::checkFieldsNotEmpty($data, ['title', 'content']);

        $adJob = new AdJob();
        $this->setCommonField($adJob, $data);

        $this->save($adJob, $this->getType());

        return $adJob;
    }

    /**
     * @throws Exception
     */
    public function update(Ad $ad, array $data): Ad
    {
        $this->checkRequiredFields($data, ['title', 'content']);
        EmptyChecker::checkFieldsNotEmpty($data, ['title', 'content']);

        $this->setCommonField($ad, $data);

        $this->flush($this->getType());

        return $ad;
    }

    public function getType(): string
    {
        return self::JOB;
    }
}
