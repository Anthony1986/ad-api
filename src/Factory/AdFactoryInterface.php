<?php

namespace App\Factory;

use App\Entity\Ad;

interface AdFactoryInterface
{
    public const ESTATE = 'estate';
    public const JOB = 'job';
    public const CAR = 'car';

    public function create(array $data): Ad;

    public function getType(): string;

    public function update(Ad $ad, array $data);
}
