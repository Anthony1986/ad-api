<?php

namespace App\Factory;

use App\Checker\EmptyChecker;
use App\Entity\Ad;
use App\Entity\AdEstate;
use Exception;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('ad_factory', ['alias' => 'ad_estate'])]
class AdEstateFactory extends AdFactory implements AdFactoryInterface
{
    /**
     * @throws Exception
     */
    public function create(array $data): Ad
    {
        $this->checkRequiredFields($data, ['title', 'content']);
        EmptyChecker::checkFieldsNotEmpty($data, ['title', 'content']);

        $adEstate = new AdEstate();
        $this->setCommonField($adEstate, $data);

        $this->save($adEstate, $this->getType());

        return $adEstate;
    }

    /**
     * @throws Exception
     */
    public function update(Ad $ad, array $data): Ad
    {
        $this->checkRequiredFields($data, ['title', 'content']);
        EmptyChecker::checkFieldsNotEmpty($data, ['title', 'content']);

        $this->setCommonField($ad, $data);

        $this->flush($this->getType());

        return $ad;
    }

    public function getType(): string
    {
        return self::ESTATE;
    }
}
