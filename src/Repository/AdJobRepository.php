<?php

namespace App\Repository;

use App\Entity\AdJob;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AdJob>
 *
 * @method AdJob|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdJob|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdJob[]    findAll()
 * @method AdJob[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdJobRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdJob::class);
    }
}
