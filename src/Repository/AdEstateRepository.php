<?php

namespace App\Repository;

use App\Entity\AdEstate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AdEstate>
 *
 * @method AdEstate|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdEstate|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdEstate[]    findAll()
 * @method AdEstate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdEstateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdEstate::class);
    }
}
