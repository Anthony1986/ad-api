<?php

namespace App\Repository;

use App\Entity\AdCar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<AdCar>
 *
 * @method AdCar|null find($id, $lockMode = null, $lockVersion = null)
 * @method AdCar|null findOneBy(array $criteria, array $orderBy = null)
 * @method AdCar[]    findAll()
 * @method AdCar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdCarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AdCar::class);
    }
}
