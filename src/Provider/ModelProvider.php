<?php

namespace App\Provider;

use App\Repository\ModelRepository;

class ModelProvider implements ModelProviderInterface
{
    public function __construct(
        private readonly ModelRepository $modelRepository,
    )
    {
    }

    public function getAllModels(): array
    {
        return $this->modelRepository->findAll();
    }
}
