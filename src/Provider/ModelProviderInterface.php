<?php

namespace App\Provider;

interface ModelProviderInterface
{
    public function getAllModels(): array;
}
