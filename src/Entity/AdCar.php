<?php

namespace App\Entity;

use App\Repository\AdCarRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdCarRepository::class)]
class AdCar extends Ad
{
    private const CAR = 'car';

    #[ORM\ManyToOne(inversedBy: 'adCars')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Brand $brand = null;

    #[ORM\ManyToOne(inversedBy: 'adCars')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Model $model = null;

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): static
    {
        $this->brand = $brand;

        return $this;
    }

    public function getModel(): ?Model
    {
        return $this->model;
    }

    public function setModel(?Model $model): static
    {
        $this->model = $model;

        return $this;
    }

    public function getType(): string
    {
        return self::CAR;
    }
}
