<?php

namespace App\Entity;

use App\Repository\AdJobRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdJobRepository::class)]
class AdJob extends Ad
{
    private const JOB = 'job';

    public function getType(): string
    {
        return self::JOB;
    }
}
