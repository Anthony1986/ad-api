<?php

namespace App\Entity;

use App\Repository\ModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: ModelRepository::class)]
#[UniqueEntity(fields: ['name'], message: 'This model already exists.')]
class Model
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'model')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Brand $brand = null;

    #[ORM\OneToMany(mappedBy: 'model', targetEntity: AdCar::class)]
    private Collection $adCars;

    public function __construct()
    {
        $this->adCars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): static
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection<int, AdCar>
     */
    public function getAdCars(): Collection
    {
        return $this->adCars;
    }

    public function addAdCar(AdCar $adCar): static
    {
        if (!$this->adCars->contains($adCar)) {
            $this->adCars->add($adCar);
            $adCar->setModel($this);
        }

        return $this;
    }

    public function removeAdCar(AdCar $adCar): static
    {
        if ($this->adCars->removeElement($adCar)) {
            // set the owning side to null (unless already changed)
            if ($adCar->getModel() === $this) {
                $adCar->setModel(null);
            }
        }

        return $this;
    }
}
