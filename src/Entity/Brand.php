<?php

namespace App\Entity;

use App\Repository\BrandRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

#[ORM\Entity(repositoryClass: BrandRepository::class)]
#[UniqueEntity(fields: ['name'], message: 'This brand already exists.')]
class Brand
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: AdCar::class)]
    private Collection $adCars;

    #[ORM\OneToMany(mappedBy: 'brand', targetEntity: Model::class)]
    private Collection $model;

    public function __construct()
    {
        $this->adCars = new ArrayCollection();
        $this->model = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, AdCar>
     */
    public function getAdCars(): Collection
    {
        return $this->adCars;
    }

    public function addAdCar(AdCar $adCar): static
    {
        if (!$this->adCars->contains($adCar)) {
            $this->adCars->add($adCar);
            $adCar->setBrand($this);
        }

        return $this;
    }

    public function removeAdCar(AdCar $adCar): static
    {
        if ($this->adCars->removeElement($adCar)) {
            // set the owning side to null (unless already changed)
            if ($adCar->getBrand() === $this) {
                $adCar->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Model>
     */
    public function getModel(): Collection
    {
        return $this->model;
    }

    public function addModel(Model $model): static
    {
        if (!$this->model->contains($model)) {
            $this->model->add($model);
            $model->setBrand($this);
        }

        return $this;
    }

    public function removeModel(Model $model): static
    {
        if ($this->model->removeElement($model)) {
            // set the owning side to null (unless already changed)
            if ($model->getBrand() === $this) {
                $model->setBrand(null);
            }
        }

        return $this;
    }
}
