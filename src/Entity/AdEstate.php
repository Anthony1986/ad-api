<?php

namespace App\Entity;

use App\Repository\AdJobRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AdJobRepository::class)]
class AdEstate extends Ad
{
    private const ESTATE = 'estate';

    public function getType(): string
    {
        return self::ESTATE;
    }
}
