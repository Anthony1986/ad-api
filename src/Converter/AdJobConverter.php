<?php

namespace App\Converter;

use App\Entity\Ad;
use Symfony\Component\DependencyInjection\Attribute\AutoconfigureTag;

#[AutoconfigureTag('ad_converter', ['alias' => 'ad_job'])]
class AdJobConverter implements ConverterInterface
{
    public function retrieveData(Ad $ad, array $data): array
    {
        return [
            'id' => $ad->getId(),
            'type' => $data['type'] ?? $ad->getType(),
            'title' => $ad->getTitle(),
            'content' => $ad->getContent(),
        ];
    }

    public function getType(): string
    {
        return self::JOB;
    }
}
