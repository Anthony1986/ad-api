<?php

namespace App\Converter;

use App\Entity\Ad;

interface ConverterInterface
{
    public const ESTATE = 'estate';
    public const JOB = 'job';
    public const CAR = 'car';

    public function retrieveData(Ad $ad, array $data): array;

    public function getType(): string;
}
