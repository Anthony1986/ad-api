<?php


namespace App\Tests\Controller;

use App\Controller\AdController;
use App\Converter\ConverterInterface;
use App\Entity\AdCar;
use App\Entity\Brand;
use App\Entity\Model;
use App\Factory\AdFactoryInterface;
use App\Resolver\AdConverterResolverInterface;
use App\Resolver\AdFactoryResolverInterface;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdControllerTest extends TestCase
{
    private MockObject|ContainerInterface $containerInterface;
    private AdConverterResolverInterface|MockObject $adConverterResolverInterfaceMock;
    private MockObject|ConverterInterface $converterInterfaceMock;
    private MockObject|AdFactoryResolverInterface $adFactoryResolverInterfaceMock;
    private LoggerInterface|MockObject $loggerInterfaceMock;
    private EntityManagerInterface|MockObject $entityManagerInterfaceMock;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->containerInterface = $this->createMock(ContainerInterface::class);
        $this->adConverterResolverInterfaceMock = $this->createMock(AdConverterResolverInterface::class);
        $this->converterInterfaceMock = $this->createMock(ConverterInterface::class);
        $this->adFactoryResolverInterfaceMock = $this->createMock(AdFactoryResolverInterface::class);
        $this->loggerInterfaceMock = $this->createMock(LoggerInterface::class);
        $this->entityManagerInterfaceMock = $this->createMock(EntityManagerInterface::class);
        $this->adFactoryInterfaceMock = $this->createMock(AdFactoryInterface::class);
    }
    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testGetAd()
    {
        $brand = $this->createMockBrand();
        $model = $this->createMockModel();
        $ad = $this->createMockAd($brand, $model);
        $resolvedData = $this->createResolvedData($ad, $brand, $model);

        $this->adConverterResolverInterfaceMock->expects($this->once())->method('resolve')->willReturn($this->converterInterfaceMock);
        $this->converterInterfaceMock->expects($this->once())->method('retrieveData')->willReturn($resolvedData);

        $controller = $this->createAdController();

        $response = $controller->get($ad);

        $expectedResponse = [
            'code' => Response::HTTP_OK,
            'message' => 'Ad retrieved successfully',
            'data' => $resolvedData,
        ];

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($expectedResponse, json_decode($response->getContent(), true));
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testCreateAd()
    {
        $this->adFactoryResolverInterfaceMock->method('resolve')->willReturn($this->createMockAdFactory());

        $ad = $this->createMockAd();
        $data = [
            'type' => 'car',
            'title' => 'un titre nouveau',
            'content' => 'Un contenu nouveau',
            'model' => 'Rs4',
        ];

        $this->adFactoryInterfaceMock->method('create')->with($data)->willReturn($ad);
        $this->adConverterResolverInterfaceMock->method('resolve')->willReturn($this->converterInterfaceMock);

        $resolvedData = $this->createResolvedData($ad);

        $this->converterInterfaceMock->method('retrieveData')->with($ad, $data)->willReturn($resolvedData);

        $controller = $this->createAdController();

        $jsonData = json_encode(['data' => $data]);
        $request = Request::create('/ad-create', 'POST', [], [], [], [], $jsonData);

        $response = $controller->create($request);
        $expectedResponse = [
            'code' => Response::HTTP_CREATED,
            'message' => 'A new Ad has been created with success',
            'data' => $resolvedData,
        ];

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($expectedResponse, json_decode($response->getContent(), true));
        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testUpdateAd()
    {
        $this->adFactoryResolverInterfaceMock->method('resolve')->willReturn($this->adFactoryInterfaceMock);

        $ad = $this->createMockAd();
        $data = [
            'type' => 'car',
            'title' => 'Updated Title',
            'content' => 'Updated Content',
        ];

        $this->adFactoryInterfaceMock->method('update')->with($ad, $data);
        $this->adConverterResolverInterfaceMock->method('resolve')->willReturn($this->converterInterfaceMock);

        $resolvedData = $this->createResolvedData($ad);

        $this->converterInterfaceMock->method('retrieveData')->with($ad, $data)->willReturn($resolvedData);

        $controller = $this->createAdController();

        $jsonData = json_encode(['data' => $data]);
        $request = Request::create('/ad-update/1', 'PUT', [], [], [], [], $jsonData);

        $response = $controller->update($request, $ad);

        $expectedResponse = [
            'code' => Response::HTTP_OK,
            'message' => 'Ad updated successfully',
            'data' => $resolvedData,
        ];

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($expectedResponse, json_decode($response->getContent(), true));
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @throws Exception
     * @throws \Exception
     */
    public function testDeleteAd()
    {
        $ad = $this->createMockAd();
        $this->entityManagerInterfaceMock->expects($this->once())->method('remove')->with($ad);
        $this->entityManagerInterfaceMock->expects($this->once())->method('flush');
        $this->loggerInterfaceMock->expects($this->never())->method('error');

        $controller = $this->createAdController();

        $response = $controller->delete($ad);

        $expectedResponse = [
            'code' => Response::HTTP_OK,
            'message' => 'Ad deleted successfully',
        ];

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals($expectedResponse, json_decode($response->getContent(), true));
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    private function createAdController(): AdController
    {
        $controller = new AdController(
            $this->adFactoryResolverInterfaceMock,
            $this->adConverterResolverInterfaceMock,
            $this->loggerInterfaceMock, $this->entityManagerInterfaceMock
        );

        $controller->setContainer($this->containerInterface);

        return $controller;
    }

    /**
     * @throws Exception
     */
    private function createMockBrand(): MockObject|Brand
    {
        $brand = $this->createMock(Brand::class);
        $brand->method('getId')->willReturn(1);
        $brand->method('getName')->willReturn('Audi');

        return $brand;
    }

    /**
     * @throws Exception
     */
    private function createMockModel(): MockObject|Model
    {
        $model = $this->createMock(Model::class);
        $model->method('getId')->willReturn(1);
        $model->method('getName')->willReturn('Rs4');

        return $model;
    }

    /**
     * @throws Exception
     */
    private function createMockAd(Brand $brand = null, Model $model = null): AdCar|MockObject
    {
        $ad = $this->createMock(AdCar::class);
        $ad->method('getId')->willReturn(1);
        $ad->method('getType')->willReturn('car');
        $ad->method('getTitle')->willReturn('new Title');
        $ad->method('getContent')->willReturn('Something');
        $ad->method('getBrand')->willReturn($brand ?: $this->createMockBrand());
        $ad->method('getModel')->willReturn($model ?: $this->createMockModel());

        return $ad;
    }

    /**
     * @throws Exception
     */
    private function createMockAdFactory(): MockObject|AdFactoryInterface
    {
        $this->adFactoryInterfaceMock = $this->createMock(AdFactoryInterface::class);
        return $this->adFactoryInterfaceMock;
    }

    /**
     * @throws Exception
     */
    private function createResolvedData(AdCar $ad, Brand $brand = null, Model $model = null)
    {
        $brand = $brand ?: $this->createMockBrand();
        $model = $model ?: $this->createMockModel();

        return [
            'id' => $ad->getId(),
            'type' => $ad->getType(),
            'title' => $ad->getTitle(),
            'content' => $ad->getContent(),
            'brand' => [
                'id' => $brand->getId(),
                'name' => $brand->getName(),
            ],
            'model' => [
                'id' => $model->getId(),
                'name' => $model->getName(),
            ],
        ];
    }
}
