<?php

namespace App\Tests\Matcher;

use App\Entity\Model;
use App\Matcher\ModelMatcher;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;

class MatcherTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testFindMatchingModel()
    {
        $modelMatcher = new ModelMatcher();

        $models = [
            'Cabriolet' => $this->createMockModel('Cabriolet'),
            'Rs3' => $this->createMockModel('Rs3'),
            'Rs4' => $this->createMockModel('Rs4'),
            'R8' => $this->createMockModel('R8'),
            'S4 Avant' => $this->createMockModel('S4 Avant'),
            'S4 Cabriolet' => $this->createMockModel('S4 Cabriolet'),
            'Serie 1' => $this->createMockModel('Serie 1'),
            'Serie 2' => $this->createMockModel('Serie 2'),
            'Serie 3' => $this->createMockModel('Serie 3'),
            'Serie 4' => $this->createMockModel('Serie 4'),
            'Serie 5' => $this->createMockModel('Serie 5'),
            'C15' => $this->createMockModel('C15'),
            'C3' => $this->createMockModel('C3'),
            'C3 Aicross' => $this->createMockModel('C3 Aicross'),
            'C3 Picasso' => $this->createMockModel('C3 Picasso'),
            'C4 Picasso' => $this->createMockModel('C4 Picasso'),
            'Ds3' => $this->createMockModel('Ds3'),
            'Ds4' => $this->createMockModel('Ds4'),
            'Ds5' => $this->createMockModel('Ds5'),
        ];

        $sameModel = $modelMatcher->findMatchingModel('C4 Picasso', $models);
        $this->assertSame($models['C4 Picasso'], $sameModel);

        $sameModel1 = $modelMatcher->findMatchingModel('C3', $models);
        $this->assertSame($models['C3'], $sameModel1);

        $similarModel = $modelMatcher->findMatchingModel('rs4 avant', $models);
        $this->assertSame($models['Rs4'], $similarModel);

        $similarModel1 = $modelMatcher->findMatchingModel('Gran Turismo Série5', $models);
        $this->assertSame($models['Serie 5'], $similarModel1);

        $similarModel2 = $modelMatcher->findMatchingModel('ds 3 crossback', $models);
        $this->assertSame($models['Ds3'], $similarModel2);

        $similarModel3 = $modelMatcher->findMatchingModel('CrossBack ds 3', $models);
        $this->assertSame($models['Ds3'], $similarModel3);

        $nullModel = $modelMatcher->findMatchingModel('ferrari', $models);
        $this->assertNull($nullModel);
    }
    /**
     * @throws Exception
     */
    private function createMockModel(string $name): Model
    {
        $model = $this->createMock(Model::class);
        $model->method('getName')->willReturn($name);

        return $model;
    }
}