# Ad-api

This project is based on Symfony 6.2 PHP 8.2. Il s'agit d'une API REST qui permet de gérer des annonces

## Pre-requisites
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Installation
1. Navigate to the project directory:

    ```bash
    cd ad-api
    ```
2. Build and start the Docker containers: :

    ```bash
    docker compose up -d --build
    ```

3. Install PHP dependencies with Composer :

    ```bash
    docker compose exec ad-api composer install
    ```

4. if database doesn't exist create the database :

    ```bash
    docker compose exec ad-api bin/console doctrine:database:create
    ```
   
5. Apply migrations :

    ```bash
    docker compose exec ad-api bin/console doctrine:migrations:migrate
    ```
   
6. Launch fixtures :

    ```bash
    docker compose exec ad-api bin/console doctrine:fixtures:load
    ```

## Usage

You can access the Symfony application by using the URL [http://localhost/](http://localhost/)

You can access phpmyadmin by using the URL [http://localhost:8080/index.php](http://localhost:8080/index.php)

## Curl
CREATE AD :
url : http://localhost/ad-create
```bash
curl -X POST http://localhost/ad-create \
-H "Content-Type: application/json" \
-d '{
  "data": {
    "type": "car",
    "title": "un titre nouveau",
    "content": "Un contenu nouveau",   
    "model": "ds 3 crossback"
  }
}'
```
UPDATE AD :
url : http://localhost/ad-update/{id}
```bash
curl -X PUT http://localhost/ad-update/1 \
-H "Content-Type: application/json" \
-d '{
  "data": {
    "type": "car",
    "title": "un titre nouveau",
    "content": "Un contenu nouveau",
    "model": "serie 8"
  }
}'
```
DELETE AD :
url : http://localhost/ad-delete/{id}
```bash
curl -X DELETE http://localhost/ad-delete/3
```

GET AD :
url : http://localhost/ad/{id}
```bash
curl http://localhost/ad/1
```

## TEST
```bash
docker compose exec ad-api ./vendor/bin/phpunit
  ```

